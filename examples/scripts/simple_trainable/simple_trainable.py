import numpy as np

from hpogrid import CustomModel

class MyTrainableClass(CustomModel):

    def initialize(self, config):
        self.timestep = 0
        self.alpha = config.get("alpha", 1)
        self.beta = config.get("beta", 1)
        print('Current Hyperparameters:')
        print('alpha: {}, beta: {}'.format(self.alpha, self.beta))
        
    def get_loss(self):
        #Easom function
        loss = -np.cos(self.alpha)*np.cos(self.beta)
        loss *= np.exp(-((self.alpha-np.pi)**2+(self.beta-np.pi)**2))
        loss *= np.tanh(float(self.timestep))
        return loss

    # step = one training iteration
    def step(self):
        self.timestep += 1
        loss = self.get_loss()
        print('loss: {}'.format(loss))
        return {"loss": loss}

    def save_checkpoint(self, checkpoint_dir):
        path = os.path.join(checkpoint_dir, "checkpoint")
        with open(path, "w") as f:
            f.write(json.dumps({"timestep": self.timestep}))
        return path

    def load_checkpoint(self, checkpoint_path):
        with open(checkpoint_path) as f:
            self.timestep = json.loads(f.read())["timestep"]



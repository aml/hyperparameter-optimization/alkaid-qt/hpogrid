#!/usr/bin/env python

import os
import argparse
import json

import numpy as np

from sklearn.metrics import roc_curve, auc

import xgboost as xgb

from tabulate import tabulate

import hpogrid
from hpogrid import CustomModel

class ttH_bdt(CustomModel):
    def load_param(self, config = None):
        # Write all parameters into the dictionary self.config
        if config is not None:
            self.config = config
        else:
            self.config = {}

        # Create variable name corresponding to keys in self.config 
        # and assign the corresponding values
        self.__dict__.update(**self.config)


    def initialize(self, config=None):

        self.load_param(config)

        data = dict(np.load(self.data_path, allow_pickle=True))
        x_train = data['x_train']
        y_train = data['y_train']
        x_val = data['x_val']
        y_val = data['y_val']
        self.dTrain = xgb.DMatrix(x_train, label = y_train, weight = weight_train.values)
        self.dVal = xgb.DMatrix(x_val, label = y_val, weight = weight_val.values)

    #    dTrain = xgb.DMatrix(train, label = y_train)
    #    dVal = xgb.DMatrix(val, label = y_val)

        # Train model
        print('Train model.')

        self.param = {
        'colsample_bytree': self.colsample_bytree,
        'eval_metric': self.eval_metric,
        'grow_policy': self.grow_policy,
        'max_delta_step': self.max_delta_step,
        'nthread': self.nthread, 
        'min_child_weight': self.min_child_weight,
        'subsample': self.subsample,
        'eta': self.eta,
        'max_bin': self.max_bin, 
        'objective': self.objective, 
        'alpha': self.alpha, 
        'tree_method': self.tree_method, 
        'max_depth': self.max_depth, 
        'gamma': self.gamma, 
        'booster': self.booster}
        print(self.param)      


    def step(self):
        evallist  = [(self.dTrain, 'train'), (self.dVal, 'eval')]
        evals_result = {}
        eval_result_history = []
        try:
            bst = xgb.train(self.param, self.dTrain, self.num_round, evals=evallist,
                early_stopping_rounds=self.early_stopping_rounds, evals_result=evals_result,
                verbose_eval=self.verbose_eval)
        except KeyboardInterrupt:
            print('Finishing on SIGINT.')

        # BDT outputs per process for correlation matrix (validation set only)

        aucValue =  max(evals_result['eval']['auc'])
        loglossR =  max(evals_result['eval']['logloss'])

        return {'auc':aucValue, 'logloss':loglossR}

from tensorflow.keras import datasets
from hpogrid import CustomModel

(train_images, train_labels), (test_images, test_labels) = datasets.cifar10.load_data()

# Normalize pixel values to be between 0 and 1
train_images, test_images = train_images / 255.0, test_images / 255.0


class CIFAR10_CNN(CustomModel):
    def initialize(self, config):
        self.hiddens = config.get('hiddens', 64)
        self.lr = config.get('lr', 1e-3)
        self.beta_1 = config.get('beta_1', 0.9)
        self.batchsize = config.get('batchsize', 64)
        import tensorflow as tf
        model = tf.keras.models.Sequential()
        model.add(tf.keras.layers.Conv2D(32, (3, 3), activation='relu', input_shape=(32, 32, 3)))
        model.add(tf.keras.layers.MaxPooling2D((2, 2)))
        model.add(tf.keras.layers.Conv2D(64, (3, 3), activation='relu'))
        model.add(tf.keras.layers.MaxPooling2D((2, 2)))
        model.add(tf.keras.layers.Conv2D(self.hiddens, (3, 3), activation='relu'))
        model.add(tf.keras.layers.Flatten())
        model.add(tf.keras.layers.Dense(64, activation='relu'))
        model.add(tf.keras.layers.Dense(10))
        model.compile(optimizer=tf.keras.optimizers.Adam(
                      learning_rate=self.lr,
                      beta_1=self.beta_1),
                      loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
                      metrics=['accuracy'])
        print(model.summary())
        self.model = model

    def step(self):
        history = self.model.fit(train_images, train_labels, epochs=10, 
                                 batch_size=self.batchsize,
                                 validation_data=(test_images, test_labels))

        test_loss, test_acc = self.model.evaluate(test_images,  test_labels, verbose=2)

        return {
            "loss": test_loss,
            "accuracy": test_acc
        }







def simple_objective(config, reporter):
    print('Current Hyperparameters:')
    print('height: {}, width: {}'.format(config['height'],config['width']))
    loss = (config["height"] - 14)**2 - abs(config["width"] - 3)
    print('loss: {}'.format(loss))
    reporter(loss=loss)

hpogrid model_config recreate FastCaloGAN_photon_200_205_model --script conditional_wgangp.py --model WGANGP --param '{"particle":"photons","pid":22,"eta_min":200,"eta_max":205}'
hpogrid search_space recreate WGANGP_space -s '{"n_disc":{"method":"uniformint","dimension":{"low":3,"high":10}}, "Lambda":{"method":"uniform","dimension":{"low":1,"high":20}},"beta1":{"method":"uniform","dimension":{"low":0.1,"high":1}},"batchsize":{"method":"categorical","dimension":{"categories": [128,256,512,1024,2048]}},"lr":{"method":"loguniform","dimension":{"low":1e-5, "high":1e-2}}}'
hpogrid hpo_config recreate random_search --algorithm random --metric loss --mode min --num_trials 1
hpogrid grid_config recreate FastCaloGAN_photon_200_205_grid_config --inDS user.chlcheng:user.chlcheng.HPO.v2.FastCaloGAN.photons.eta_200_205.dataset --site ANALY_MWT2_GPU,DESY-HH_GPU,ANALY_BNL_GPU_ARC,ANALY_MANC_GPU_TEST
hpogrid project create FastCaloGAN_photon_200_205 --model_config FastCaloGAN_photon_200_205_model --search_space WGANGP_space --hpo_config random_search --grid_config FastCaloGAN_photon_200_205_grid_config --scripts_path ${HPOGRID_BASE_PATH}/examples/scripts/FastCaloGAN


#"VoxelNormalisation":{"method":"categorical","dimension":{"categories": ["Energy","LogEnergy"]}},"LabelDefinition":{"method":"categorical","dimension":{"categories": ["LogE","MaxE"]}}

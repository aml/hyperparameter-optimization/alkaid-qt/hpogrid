hpogrid model_config recreate simple_objective_model --script simple_objective.py --model simple_objective
hpogrid search_space recreate simple_objective_space -s '{"height":{"method":"uniform","dimension":{"low":-100,"high":100}}, "width":{"method":"uniform","dimension":{"low":0,"high":20}}}'
hpogrid hpo_config recreate idds_test_config --algorithm hyperopt --metric loss --mode min --num_trials 3 --max_concurrent 3
hpogrid grid_config recreate idds_grid_config --site ANALY_CERN-PTEST
hpogrid project create idds_test --model_config simple_objective_model --search_space simple_objective_space --hpo_config idds_test_config --grid_config idds_grid_config --scripts_path ${HPOGRID_BASE_PATH}/examples/scripts/simple_objective

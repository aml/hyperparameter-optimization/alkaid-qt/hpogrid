hpogrid model_config recreate simple_trainable_model --script simple_trainable.py --model MyTrainableClass
hpogrid search_space recreate simple_trainable_space -s '{"alpha":{"method":"uniform","dimension":{"low":-20,"high":20}}, "beta":{"method":"uniform","dimension":{"low":-20,"high":20}}}'
hpogrid hpo_config recreate skopt_min_loss_20_timestep --algorithm hyperopt --metric loss --mode min --num_trials 400 --stop '{"training_iteration": 20}'
hpogrid grid_config recreate default  --site ANALY_MWT2_GPU,DESY-HH_GPU,ANALY_BNL_GPU_ARC,ANALY_MANC_GPU_TEST
hpogrid project create simple_trainable --model_config simple_trainable_model --search_space simple_trainable_space --hpo_config skopt_min_loss_20_timestep --grid_config default --scripts_path ${HPOGRID_BASE_PATH}/examples/scripts/simple_trainable

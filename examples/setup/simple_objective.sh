hpogrid model_config recreate simple_objective_model --script simple_objective.py --model simple_objective
hpogrid search_space recreate simple_objective_space -s '{"height":{"method":"uniform","dimension":{"low":-100,"high":100}}, "width":{"method":"uniform","dimension":{"low":0,"high":20}}}'
hpogrid hpo_config recreate random_search_min_loss --algorithm hyperopt --metric loss --mode min --num_trials 200 --max_concurrent 3 
hpogrid grid_config recreate default --site ANALY_MWT2_GPU,DESY-HH_GPU,ANALY_BNL_GPU_ARC,ANALY_MANC_GPU_TEST
hpogrid project create simple_objective --model_config simple_objective_model --search_space simple_objective_space --hpo_config random_search_min_loss --grid_config default --scripts_path ${HPOGRID_BASE_PATH}/examples/scripts/simple_objective

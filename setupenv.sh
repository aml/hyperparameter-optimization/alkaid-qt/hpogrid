#!/bin/bash

if [ "$#" -ge 1 ];
then
	EnvironmentName=$1
else
	EnvironmentName="default"
fi


# set up environmental paths
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done


DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"

if [ "$EnvironmentName" = "default" ]; #Default is python3
then
	setupATLAS
	lsetup "views LCG_101 x86_64-centos7-gcc8-opt"
	export PATH=${DIR}/bin:${PATH}
	export PYTHONPATH=${DIR}:${PYTHONPATH}	
elif [[ "$EnvironmentName" = "dev" ]];
then
	export PATH=/afs/cern.ch/work/c/chlcheng/local/miniconda/envs/ml-base/bin:$PATH
	export PATH=${DIR}/bin:${PATH}
	export PYTHONPATH=${DIR}:${PYTHONPATH}
fi

export HPOGRID_BASE_PATH=${DIR}
